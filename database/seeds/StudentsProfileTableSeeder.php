<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentsProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 50; $i++)
        {
            $email = $faker->unique()->freeEmail;
            $matricula = $faker->unique()->swiftBicNumber;

            $id = DB::table('students')->insertGetId([
                'matricula' => $matricula,
                'nombre' => $faker->firstName(),
                'apellidos' => $faker->lastName,
                'genero' => $faker->randomElement(['M', 'F']) 
            ]);

            DB::table('profiles')->insert([
                'email' => $email,
                'telefono' => $faker->tollFreePhoneNumber,
                'student_id' => $id
            ]);
        }
    }
}
