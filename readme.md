# Api Lumen

Api Lumen con proposito educativos desarrollado por gen7.

## Requeriemientos
* Servidor Web con PHP
* Motor de base de datos MySQL Server
* Postman

Los endpoint fueron probados con la aplicación de postman para validar la funcionalidad del servicio rest.

## Migrar base de datos

Para migrar la base de datos debemos ejecutar el siguiente comando:
* php artisan migration

Para Migrar datos de pruebas ejecute el siguiente comando
* php artisan db:seed --class=StudentsProfileTableSeeder

## Git Branches
> facade-db