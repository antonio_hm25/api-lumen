<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller 
{
    public function all(Request $request)
    {
        $queryParams = $request->all();
        $query = $this->commonQuery();

        if (count($queryParams) > 0) 
        {
            if (isset($queryParams['nombre']))
            {
                $query->where('nombre', 'like', "%{$queryParams['nombre']}%");
            }

            if (isset($queryParams['email']))
            {
                $query->where('email', 'like', "%{$queryParams['email']}%");
            }
        }

        $result = $query->get();

        return response()->json($result);
    }

    public function findBy(int $id) 
    {
        $result = $this->getStudent($id);

        return response()->json($result);
    }

    public function store(Request $request) 
    {
        $params = $request->all();

        $id = DB::table('students')->insertGetId([
            'matricula' => $params['matricula'],
            'nombre' => $params['nombre'],
            'apellidos' => $params['apellidos'],
            'genero' => $params['genero']
        ]);

        DB::table('profiles')->insert([
            'email' => $params['email'],
            'telefono' => $params['telefono'],
            'student_id' => $id
        ]);

        return $this->findBy($id);
    }

    public function update(Request $request, int $id)
    {
        $params = $request->all();

        $students = DB::table('students')->where('id', $id);
        $profiles = DB::table('profiles')->where('student_id', $id);

        $students->update([
            'nombre' => $params['nombre'],
            'apellidos' => $params['apellidos'],
            'genero' => $params['genero']
        ]);

        $profiles->update([
            'email' => $params['email'],
            'telefono' => $params['telefono']
        ]);

        return $this->findBy($id);
    }

    public function destroy(int $id)
    {
        $result = $this->getStudent($id);

        DB::table('students')->where('id', $id)->delete();

        return response()->json($result);
    }

    private function commonQuery() 
    {
        return DB::table('students')
                ->join('profiles', 'profiles.student_id', '=', 'students.id');
    }

    private function getStudent(int $id)
    {
        return $this->commonQuery()
                    ->where('students.id', $id)
                    ->first();
    }
}