<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('v1/students', 'StudentController@all');
$router->get('v1/students/{id}', 'StudentController@findBy');
$router->post('v1/students', 'StudentController@store');
$router->patch('v1/students/{id}', 'StudentController@update');
$router->delete('v1/students/{id}', 'StudentController@destroy');